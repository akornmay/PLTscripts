#!/nfshome0/lumipro/brilconda/bin/python

import tables, pandas as pd, pylab as py, sys, numpy
import struct



#filen = sys.argv[1]
filen = "/brildata/17/6082/6082_301142_1708141219_1708141811.hd5"
print "Reading from file" + filen

#channel = sys.argv[2]

h5file = tables.open_file(filen)

hist = h5file.root.pltaggzero

#number of active channels
nchannels = 16

#sumhist = numpy.zeros(14256)
calLumi = numpy.zeros((nchannels,3564))
bxLumi = numpy.zeros(3564)
totalLumi = []
elements = numpy.zeros(nchannels)

scTotalLumi = [[] for i in range(nchannels)]


sigvis = 297.
k = 11246/sigvis # this will be a by channel value later

# lets make a dictionary with the sigma_vis values
kvalue = [340,326,297,245,263,280,304,321,274,304,333,350,322,290,263,253]

print len(kvalue)




for row in hist.iterrows():
    binaryData = [struct.pack('i', i) for i in row['data']]
    fixedData = [struct.unpack('f', b)[0] for b in binaryData]

    calLumi[int(row['channelid'])] = -1 * k * numpy.log([x/16384.0 for x in fixedData])
    elements[int(row['channelid'])] = elements[int(row['channelid'])] +1
    scTotalLumi[int(row['channelid'])].append(sum(calLumi[int(row['channelid'])])*sigvis/kvalue[int(row['channelid'])])
    #once we have all channels we can sum them up for the bxlumi
    if int(row['channelid']) == 15:
        #lumi on a bx basis 
        bxLumi = [float(sum(x))/float(nchannels) for x in calLumi]
        #lumi on an per orbit basis
        totalLumi.append(sum(bxLumi))


#    totalCalLumiHist[int(row['channelid'])] = numpy.add(totalCalLumiHist[int(row['channelid'])], -1 * k * numpy.log(fixedData))
#    print totalCalLumiHist


    if numpy.all(elements%1000 == numpy.zeros(nchannels)):
        print elements[0]
#    if elements[15] == 1000:
#        break

h5file.close()

print 'Number of histograms found: '+ str(elements[0])
print len(totalLumi)
print len(scTotalLumi[4])
#avghist = sumhist/elements

#print [x for x in avghist]

py.plot(range(int(elements[0])),totalLumi,'k.-')
py.plot(range(int(elements[0])),scTotalLumi[1],'g.-')
py.plot(range(int(elements[0])),scTotalLumi[2],'r.-')
py.plot(range(int(elements[0])),scTotalLumi[4],'b.-')
py.plot(range(int(elements[0])),scTotalLumi[5],'c.-')
py.plot(range(int(elements[0])),scTotalLumi[6],'m.-')
py.plot(range(int(elements[0])),scTotalLumi[7],'g^-')
py.plot(range(int(elements[0])),scTotalLumi[8],'r^-')
py.plot(range(int(elements[0])),scTotalLumi[9],'b^-')
py.plot(range(int(elements[0])),scTotalLumi[10],'c^-')
py.plot(range(int(elements[0])),scTotalLumi[11],'m^-')
py.plot(range(int(elements[0])),scTotalLumi[12],'gs-')
py.plot(range(int(elements[0])),scTotalLumi[13],'rs-')
py.plot(range(int(elements[0])),scTotalLumi[14],'bs-')
py.plot(range(int(elements[0])),scTotalLumi[15],'cs-')


py.xlabel('Time (arbitrary values)')
py.ylabel('Lumi')

py.title("File: "+filen, fontsize=10, verticalalignment='center')
#py.title("File: "+filen, fontsize=10)

py.show()
