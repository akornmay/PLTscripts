#!/usr/bin/python

import json
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib.backends.backend_pdf import PdfPages

#scanDataFile = "/Users/Andreas/Work/PLT/2017_Data/scanData.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2017_Data/scanData_20171024.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2017_Data/scanData_20171024_cut.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2017_Data/scanData_20170807.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2017_Data/scanData_20171103.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2017_Data/scanData_20171110.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180413.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180510.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180519.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180523.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180529.txt"
#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180604.txt"
scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/scanData_20180609.txt"



with open(scanDataFile) as input:
    data = zip(*(line.strip().split('\t') for line in input))
    
print data

for channel in data[2:len(data)]:

#    print channel
    if channel[0] == 'Channel 7':
        continue
#    fit1 = np.polyfit([float(data[0][i]) for i in range(2,5)],[float(channel[i]) for i in range(2,5)],1)
    #fit range used for first scan 
#    fit1 = np.polyfit([float(data[0][i]) for i in range(2,6)],[float(channel[i])/float(channel[len(channel)-1])  for i in range(2,6)],1)
    #fit range to be used since second scan
    fit1 = np.polyfit([float(data[0][i]) for i in range(2,8)],[float(channel[i])/float(channel[len(channel)-1])  for i in range(2,8)],1)
#    print fit1

    #fit range for 20170807 data
    #fit1 = np.polyfit([float(data[0][i]) for i in range(2,4)],[float(channel[i])/float(channel[len(channel)-1])  for i in range(2,4)],1)



    fit2 = np.polyfit([float(data[0][i]) for i in range(len(data[0])-5,len(data[0]))],[float(channel[i])/float(channel[len(channel)-1]) for i in range(len(data[0])-5,len(data[0]))],1)
#    print fit2

    func1 = np.poly1d(fit1)
    func2 = np.poly1d(fit2)
    xp = np.linspace(150, 1000, 100)
    

    plt.plot([data[0][i] for i in range(1,len(data[0]))],[float(channel[i])/float(channel[len(channel)-1]) for i in range(1,len(channel))],'ro',xp,func1(xp),'-',xp,func2(xp),'-')
    plt.title(channel[0])
    plt.xlabel(data[0][0])
    plt.ylabel('Normalised Raw Rate')
    plt.axis([150, 1000, 0, 1.2])

    
    x = (func2[0] -func1[0])/(func1[1]-func2[1])
    print "U_dep for " + channel[0] + " = " + str(int(x)) + " V"
    plt.savefig("plots/" + channel[0].replace(' ','_'))
    
    plt.clf()
#plt.show()


