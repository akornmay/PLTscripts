#!/nfshome0/lumipro/brilconda/bin/python

import os
import json
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import csv
from matplotlib.backends.backend_pdf import PdfPages
import tables, pandas as pd, pylab as py, sys

#scanDataFile = "/Users/Andreas/Work/PLT/2018_Data/example_1.txt"
scanDataFile = "/nfshome0/akornmay/PLT/PLTscripts/Scan_2018_7_19_14_50_16.txt"

#create a dictionary to translate HV power supply channel names to FED channel names

HVPowerNamesToFEDChannels = {'HpFT0':19, 'HpFT1':20, 'HpFT2':22, 'HpFT3':23, 'HpNT0':13, 'HpNT1':14, 'HpNT2':16, 'HpNT3':17, 'HmFT0':7, 'HmFT1':8, 'HmFT2':10, 'HmFT3':11, 'HmNT0':1, 'HmNT1':2, 'HmNT2':4, 'HmNT3':5}

FEDChannelsUsed = []
FEDChannelsUsedSorted = []
Variables = []
Scanned = []
ScannedVariables = []
ChannelN = -1
data = []

with open(scanDataFile) as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='>')
    for row in reader:
        if len(row) > 0:
            if row[1] in HVPowerNamesToFEDChannels:                
                FEDChannelsUsed = [ HVPowerNamesToFEDChannels[x] for x in row[1:]]
                FEDChannelsUsedSorted = sorted(FEDChannelsUsed)
            elif row[1] == '#channels':
                Variables = row[1:]
                if 'DIP(telescope+instLumi)' in Variables:
                    #here we need to massage the list of variables a bit. This could be fixed in the log file
                    index = Variables.index('DIP(telescope+instLumi)')
                    Variables.insert(index,'DIP telescope')
                    Variables.insert(index,'DIP instlumi')
                    del Variables[index+2]                    
                Scanned = next(reader)[1:]
            elif list(row[1])[0] == '#':
                data.append(row)
       
print FEDChannelsUsed
#print FEDChannelsUsedSorted
#print Variables
#print Scanned

ChannelN = int(Scanned[0])

for ii in range(len(Scanned)):
    if Scanned[ii] == '1':
        ScannedVariables.append(Variables[ii])

print ScannedVariables

#print data

#The important information is now extracted from the log file
#Now we try to make some plots

times= [x[0] for x in data]
timestamps = [int(time.mktime(time.strptime(x[0], '%Y.%m.%d %H:%M:%S.%f'))) for x in data]
print "Number of measurements: " + str(len(timestamps))

scanstep = [''.join(list(x[1])[1:]) for x in data if list(x[1])[1] != 'M']
print scanstep
setvoltage = [x[2] for x in data if list(x[1])[1] != 'M']
print setvoltage

#take out all the scanned variables
scandata = []
scanpoint = -1
for x in data:
    templist = []
    templist.append(int(time.mktime(time.strptime(x[0], '%Y.%m.%d %H:%M:%S.%f'))))
    if list(x[1])[1] != 'M':
        scanpoint = ''.join(list(x[1])[1:])
        templist.append(scanpoint)
        for ii in range(len(ScannedVariables)):
            templist.append(x[((ii)*ChannelN)+2:((ii+1)*ChannelN)+2])
        templist.append(x[-1])
    if list(x[1])[1] == 'M':
        templist.append(scanpoint)
        for ii in range(len(ScannedVariables)):
            #The scan step configurations are not being repeated, so I fake them back into each row
            if (ii == 0 or ii == 1):
                fakelist = scandata[-1][ii+2]
                templist.append(fakelist)
            else:
                templist.append(x[((ii-2)*ChannelN)+2:((ii-1)*ChannelN)+2])
        templist.append(x[-1])
    scandata.append(templist)


#let's load the HD5 file
filen = sys.argv[1]
channel = sys.argv[2]

h5file = tables.open_file(filen)
node = h5file.root.pltaggzero


#idea how to do things: run over data from HD5file, replace missing per channel data in scandata list
#then do plots from scan data -> minimal changes when running from log file only
#scandata = scandata[-5:]
for x in scandata:
    tmplist = []
    print x[0]
    for row in node.iterrows():
        if row['timestampsec'] == x[0]:
            #row[data] contains number of zeros for 4*4096 orbits
            coincidences = [16384-y for y in row['data']]
#            callumi = -1 * k * np.log([y/16384.0 for y in row['data']])
            tmplist.append(sum(coincidences)/8.0)
#            tmplist.append(sum(row['data']))
    if len(tmplist) == ChannelN:
        x[6] = tmplist
    elif len(tmplist) == 0:
        for row in node.iterrows():
            if row['timestampsec'] == x[0]+1:
                #row[data] contains number of zeros for 4*4096 orbits
                coincidences = [16384-y for y in row['data']]
            #            callumi = -1 * k * np.log([y/16384.0 for y in row['data']])
                tmplist.append(sum(coincidences)/8.0)
#            tmplist.append(sum(row['data']))
        if len(tmplist) == ChannelN:
            x[6] = tmplist
        
#print scandata
emptylist = []
for ii in range(ChannelN):
    emptylist.append('0')
print "number of scan points without valid data: " + str(len([x for x in scandata if x[6]==emptylist]))
#let's throw out the bad data points
#print scandata
print len(scandata)

scandata = [x for x in scandata if x[6]!=emptylist]
print len(scandata)
tmplist = []
#let's only keep the points where the measured voltage is close to the desired set voltage
for mp in scandata:
     for channel in range(ChannelN):
        if int(mp[2][channel])*1.05 < float(mp[4][channel]) or int(mp[2][channel])*0.95 > float(mp[4][channel]):
            #if one of the values is out of a 5% range we kick out the measurement point
            break
        elif channel == ChannelN -1:
            tmplist.append(mp)
        else:
            continue

scandata = tmplist                
print len(scandata)

# we need to correct for the drop in lumi over the time of the scan
# so we take the value 5min before the scan and 5min after the scan and assume a linear drop
# get the lumi at starting point
startinglumi = []
print scandata[0][0]
offset = 300
while len(startinglumi) != ChannelN :
    print 'offset : ' + str(offset)
    for row in node.iterrows():
        if row['timestampsec'] == scandata[0][0] - offset:
            #row[data] contains number of zeros for 4*4096 orbits
            coincidences = [16384-y for y in row['data']]
            startinglumi.append(sum(coincidences)/8.0)
    if len(startinglumi) == ChannelN:
        break
    else:
        startinglumi = []
        offset += 1

print len(startinglumi)        
print startinglumi

endinglumi = []
print scandata[-1][0]
offset = 300
while len(endinglumi) != ChannelN :
    print 'offset : ' + str(offset)
    for row in node.iterrows():
        if row['timestampsec'] == scandata[0][0] + offset:
            #row[data] contains number of zeros for 4*4096 orbits
            coincidences = [16384-y for y in row['data']]
            endinglumi.append(sum(coincidences)/8.0)
    if len(endinglumi) == ChannelN:
        break
    else:
        endinglumi = []
        offset += 1

print len(endinglumi)        
print endinglumi


#TO_DO
#calculate a correction factor w.r.t. the time in the timestamps
#do the normalization of all channels






#now let's create a single measurement point for each voltage step
tmplist = []
scandatasingle = []
for step in scanstep:
    #print step
    #we separate the list by scan steps and convert everything to values we can average str -> int/float
    tmplist = [x for x in scandata if x[1] == step]
    #print "tmplist size: " +str(len(tmplist))
    tmplist2 = []
    if len(tmplist) > 0:
        for mp in tmplist:
            tmplist3 = [] 
            for ii in range(len(mp)):
                #print len(mp)
                #print ii
                #print type(mp[ii])
                if type(mp[ii]) == list:
                    #print [float(y) for y in mp[ii]]
                    tmplist3.append([float(y) for y in mp[ii]])
                elif type(mp[ii]) == str:
                    #print float(mp[ii])
                    tmplist3.append(float(mp[ii]))
                else:
                    #print mp[ii]
                    tmplist3.append(mp[ii])
            tmplist2.append(tmplist3)
    #print "tmplist2 size: " + str(len(tmplist2))
    #print tmplist2
    #now we do the averaging, I assume that all elements of the list have the same length (fingers crossed)
    tmplist4 = []
    if len(tmplist2) > 0:
        #print "averaging"
        for ii in range(len(tmplist2[0])):
            #print ii
            if type(tmplist2[0][ii]) == list:
                tmplist5 = np.zeros(ChannelN)
                for mplist in [x[ii] for x in tmplist2]:
                    tmplist5 = tmplist5 + mplist
                #print tmplist5 / len([x[ii] for x in tmplist2])
                tmplist4.append(tmplist5 / len([x[ii] for x in tmplist2]))
            else:
                #print np.mean([x[ii] for x in tmplist2])
                tmplist4.append(np.mean([x[ii] for x in tmplist2]))
        scandatasingle.append(tmplist4)
#print "scandatasingle"
#print scandatasingle







#now we do several things:
# 1) make fits in the low and high region
# 2) plot everything
# 3) save plot

foldername = time.strftime('%Y%m%d',time.localtime(int(scandata[0][0])))
print foldername

for jj in range(ChannelN):
    #plot rate against time
#    plt.plot([x[0] for x in scandata], [x[5][jj] for x in scandata],'ro')
    #lower range fit
    fit1 = np.polyfit([x[4][jj] for x in scandatasingle if x[4][jj] > 305 and x[4][jj] < 450],[x[6][jj] for x in scandatasingle if x[4][jj] > 305 and x[4][jj] < 450],1)
    func1 = np.poly1d(fit1)
    #upper range fit
    fit2 = np.polyfit([x[4][jj] for x in scandatasingle if x[4][jj] > 500 and x[4][jj] < 1000],[x[6][jj] for x in scandatasingle if x[4][jj] > 500 and x[4][jj] < 1000],1)
    func2 = np.poly1d(fit2)

    xp = np.linspace(150, 1000, 100)

#    plt.plot([x[4][jj] for x in scandata], [x[6][jj] for x in scandata],'ro')
    plt.plot([x[4][jj] for x in scandatasingle], [x[6][jj] for x in scandatasingle],'ro',xp,func1(xp),'-',xp,func2(xp),'-')
    
    plt.xlabel(ScannedVariables[2])
    plt.ylabel('raw rate/nibble')
    plt.title('Channel ' + str(FEDChannelsUsed[jj]))
    
    #calculate the intersection point of the two fits
    x = (func2[0] -func1[0])/(func1[1]-func2[1])
    print "U_dep for " + str(FEDChannelsUsed[jj]) + " = " + str(int(x)) + " V"

    try:
        os.makedirs(foldername)
    except:
        pass

    plt.savefig( foldername + '/Channel_' + str(FEDChannelsUsed[jj]) )
    plt.clf()
#plt.show() 





h5file.close()


