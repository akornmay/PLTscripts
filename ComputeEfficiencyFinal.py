#!/usr/bin/python

import json
import time
import datetime
import numpy

allPLTFileName = "/Users/Andreas/Work/PLT/2017_Data/FILL6012-6417.json"

allPLTFile = open(allPLTFileName, 'r')
allPLTData = allPLTFile.read()
parsedALL = json.loads(allPLTData[3:])

for sigVisData in parsedALL:
    if "PLT" in sigVisData['name']:
        print "Results for "+sigVisData['name']
        sigVisX = sigVisData['x']
        sigVisY = sigVisData['y']
        sigVisFill = sigVisData['_other']['fill']

mylist = zip(sigVisFill,sigVisX,sigVisY,[ time.mktime(datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').timetuple()) for i in sigVisX])
for i in mylist[::-1]:
    print str(i[0]) + " " +  datetime.datetime.strptime(i[1], '%Y-%m-%dT%H:%M:%S.%fZ').strftime("%d/%m/%y  %H:%M:%S") \
        + " " +  str(i[2]) + " " + str(i[3])

print len(mylist)
# for i in mylist[::-1]:
#     print str(i[0]) 
# for i in mylist[::-1]: #for i in [x for x in mylist if '150' in x[2]][::-1]:
# #    print str(i[1]) 
#     print datetime.datetime.strptime(i[1], '%Y-%m-%dT%H:%M:%S.%fZ').strftime("%d/%m/%y  %H:%M:%S")
# #    print time.mktime(datetime.datetime.strptime(i[3], '%Y-%m-%dT%H:%M:%S.%fZ').timetuple())
# for i in mylist[::-1]: #for i in [x for x in mylist if '150' in x[2]][::-1]:
#     print str(i[2]) 

#
# Apply a cut to the fit points
# We only want measurements between FILL 6018 and 6053
#

intervals = [[6017,6053],
            [6155,6194],
            [6236,6293]]

goalval = [293,299,301]

count = 0
for interval in intervals:
#    print interval
#    print "looking at data between " + str(interval[0]) + " and " + str(interval[1])
    data = [x for x in mylist if (x[0] > interval[0] and x[0] < interval[1]) ]
#    print len(data)
    #
    # Now use the timestamps as x in fit
    #

    fit = numpy.polyfit([i[3] for i in data],[i[2] for i in data] , 1)
    func = numpy.poly1d(fit)

    # conversion from sigVis to coefficient in yaml file
    #k=11246.0/sigmavis
    #format for yaml file
    #payload: {'coefs': '0.000, 37.485, 0.000000'}

    for i in data:
        corr = goalval[count] - func(i[3])
        print str(i[0]) + ";" +  datetime.datetime.strptime(i[1], '%Y-%m-%dT%H:%M:%S.%fZ').strftime("%d/%m/%y  %H:%M:%S") \
            + ";" +  str(i[2]) + ";" + str(i[3]) + ";" + str(func(i[3])) + ";" + "payload: {'coefs': '0.000, " + str(11246.0/func(i[3])) +", 0.000000'}" + ";" + str(i[2] + corr)
    count +=1
        

