#!/usr/bin/python

import json
import time
import datetime
import numpy
import numpy as np
import matplotlib.pyplot as plt
import csv

########### Small helper function in case a cell is not filled 
def FloatOrZero(value):
    try:
        return float(value)
    except:
        return 0.0


trainPLTFileName = "/Users/Andreas/Work/PLT/2017_Data/LinearityStudy6016-6371_train.csv"
train_sigvis = []
train_fill = []
train_SBIL = []

with open(trainPLTFileName,'rU') as trainPLTFile:
    traindata = csv.reader(trainPLTFile,dialect=csv.excel_tab, delimiter=',')
    next(traindata, None) 
    for row in traindata:
        fill = int(row[2])
        sigvis = FloatOrZero(row[18])
        sbil = FloatOrZero(row[6])
        train_fill.append(fill)
        train_sigvis.append(sigvis)
        train_SBIL.append(sbil)
train_all = zip(train_fill,train_sigvis,train_SBIL)

leadPLTFileName = "/Users/Andreas/Work/PLT/2017_Data/LinearityStudy6016-6371_leading.csv"
lead_sigvis = []
lead_fill = []

with open(leadPLTFileName,'rU') as leadPLTFile:
    leaddata = csv.reader(leadPLTFile,dialect=csv.excel_tab, delimiter=',')
    next(leaddata, None) 
    for row in leaddata:
        fill = int(row[2])
        sigvis = FloatOrZero(row[18])
        sbil = FloatOrZero(row[6])
        lead_fill.append(fill)
        lead_sigvis.append(sigvis)
        train_SBIL.append(sbil)
lead_all = zip(lead_fill,lead_sigvis,train_SBIL)

plt.figure(0)
plt.plot([x[0] for x in train_all],[x[1] for x in train_all],'ro')
#plt.figure(1)
#plt.plot([x[0] for x in lead_all],[x[1] for x in lead_all],'g*')

intervals = [[6017,6053],
            [6155,6194],
            [6236,6293]]

functions = []
for interval in intervals:
    data = [x for x in train_all if (x[0] >= interval[0] and x[0] <= interval[1]) ]
    #do a fit to points in range
    fit = np.polyfit([i[0] for i in data],[i[1] for i in data] , 1)
    func = np.poly1d(fit)
    functions.append(func)
    #use fit as correction
        
    #plot fit
    xp = np.linspace(interval[0], interval[1], (interval[1] - interval[0]) * 10)
    plt.figure(0)
    plt.plot(xp,func(xp),'-')

print len(functions)

#apply correction functions in choosen intervals

#goalval = [297,299,298]
goalval = [298.3,298.3,298.3]

train_all_corr = [list(x) for x in train_all]
count = 0 
for interval in intervals:
    for x in train_all_corr:
        if (x[0] >= interval[0] and x[0] <= interval[1]) :
            x[1] = x[1] + (goalval[count] - functions[count](x[0]))
            x.append((x[1] + (goalval[count] - functions[count](x[0])))/x[1])
    count += 1

#print len(train_all)
#print len(train_all_corr)

print train_all
print train_all_corr

#for i in range(len(train_all)):
#    if train_all[i][1] != train_all_corr[i][1]:
#        print train_all[i],
#        print train_all_corr[i]

plt.figure(1)
plt.plot([x[0] for x in train_all_corr],[x[1] for x in train_all_corr],'ro')


for ii in range(2):
    plt.figure(ii)
    plt.xlabel("FILL")
    plt.ylabel("SigVis")
    plt.axis([6000, 6400, 270, 315])
    plt.savefig("plots/Eff_corr_" + str(ii))


plt.show()



#let's try to correllate fill and run numbers and we can directly print the IOV tag 
#read fill report data
FillReportNameList = []
FillReportNameList.append("/Users/Andreas/Work/PLT/2017_Data/FillReport_6016-6267.csv")
FillReportNameList.append("/Users/Andreas/Work/PLT/2017_Data/FillReport_6268-6467.csv")
fill = []
runNum = []
for FillReportName in FillReportNameList:
    with open(FillReportName,'rU') as FillReportFile:
        filldata = csv.reader(FillReportFile,dialect=csv.excel_tab, delimiter=',')
        next(filldata, None) 
        for row in filldata:
            fill.append(int(row[0]))
            runNum.append(row[24].split())

fill_all = [list(a) for a in zip(fill,runNum)]
fill_all.sort(key=lambda x:x[0])
fill_dict = {fill[0]:fill[1] for fill in fill_all}

print fill_dict[6061][0]
#print fill_dict

#linearity corrections from David
avgSlope = [0.00714, 0.00787]
avgSigVis = 298.3
k = 11245.5/avgSigVis

#correllate
temp_fill = -12345
for x in train_all_corr:
    if x[0] != temp_fill:
        print '    -',
        try:
            print str(fill_dict[x[0]][0]) + ":"
            print "       func: poly1d"
            print "       payload: {'coefs': '",
            if x[0] < 6162:
                print "%0.4f" % (-k*k*avgSlope[0]) ,
            else:
                print "%0.4f" % (-k*k*avgSlope[1]) ,
            print ",",
            #print "%0.4f" % (11245.5/x[1]),
            try:
                print "%0.4f" % (k*x[3]),
#                print "--> " + str(x[3]),
            except:
                print "%0.4f" % (k*1.0),
 #               print "--> " + str(1.0),
            print ",",
            print "0.0000'}"
            if x[0] < 6162:
                print "       comments: 2017, egev 6500, PROTPHYS, magnet ON, longer trains"
            else:
                print "       comments: 2017, egev 6500, PROTPHYS, magnet ON, 8b4e trains, leading 1/8 "
        except:
            print str(x[0]) + " doesn't exist in"
    temp_fill = x[0]

quit()
