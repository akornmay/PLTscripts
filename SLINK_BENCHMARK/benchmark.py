#!/usr/bin/python

import json
import time
import datetime
import numpy
import numpy as np
import matplotlib.pyplot as plt
import csv

########### Small helper function in case a cell is not filled 
def FloatOrZero(value):
    try:
        return float(value)
    except:
        return 0.0

#this part loads the VDM data

VDMdir = "/Users/Andreas/Work/PLT/software/PLTscripts/SLINK_BENCHMARK/VDMANALYZE"

allPLTFileName = VDMdir + "/PLT_const_2017.csv"
trainPLTFileName = VDMdir + "/PLT_const_leading_2017.csv"
leadingPLTFileName = VDMdir + "/PLT_const_train_2017.csv"

all_slope = []
all_fill = []
all_sbil = []

with open(allPLTFileName,'rU') as allPLTFile:
#with open(leadingPLTFileName,'rU') as allPLTFile:
    alldata = csv.reader(allPLTFile,dialect=csv.excel_tab, delimiter=',')
    next(alldata, None) 
    for row in alldata:
        fill = int(row[2])
        slope = FloatOrZero(row[18])
        sbil = FloatOrZero(row[9])
        all_fill.append(fill)
        all_slope.append(slope)
        all_sbil.append(sbil)
all_all = zip(all_fill,all_slope,all_sbil)

#plt.figure(0)
#plt.plot([x[0] for x in all_all],[x[1] for x in all_all],'ro')
#plt.figure(1)
#plt.plot([x[0] for x in lead_all],[x[1] for x in lead_all],'g*')

#plt.show()

#this part loads the data from DAVID's file

DavidFileName = VDMdir + "/Trains.csv"

david_fill = []
david_efficiency = []

with open(DavidFileName,'rU') as DavidFile:
    DavidData = csv.reader(DavidFile,dialect=csv.excel_tab, delimiter=',')
    for row in DavidData:
        david_fill.append(int(row[3]))
        david_efficiency.append(FloatOrZero(row[32]))


david_all = zip(david_fill,david_efficiency)


#this part loads the SLINK data

SLINKdir = "/Users/Andreas/Work/PLT/software/PLTscripts/SLINK_BENCHMARK/SLINK_DATA"

interval = [0,9]
functions = []

fill_numbers = [6024,
6044,
6052,
6082,
6104,
6140,
6147,
6161,
6174,
6177,
6182,
6186,
6240,
6245,
6266,
6275,
6287,
6288,
6298,
6304,
6325]
#fill_numbers = [6161]

for fill in fill_numbers:

    SLINKFileName = SLINKdir + "/CombinedRates_" + str(fill) +".txt"
    with open(SLINKFileName,'rU') as slinkFile:
        slinkdata = [x.strip().split() for x in slinkFile.readlines()]
        #get rid of first line that only holds the number of lines and the number of colliding bunches
    nBunches = int(slinkdata[0][1])
    del slinkdata[0]
    #now we calculate the SBIL values and the correction values

    slink_corr = []
    slink_sbil = []

    for point in slinkdata:
        if float(point[7]) > 0 and float(point[6]) > 20000:
            slink_sbil.append( float(point[6]) / ( int(point[5]) * nBunches ) )
 #           print float(point[6]) / ( int(point[5]) * nBunches ),
 #           print " " ,
 #           print "3:" + point[3],
 #           print "4:" + point[4],
            accrate = 1.0 - ( float(point[4]) / float(point[3]) )
 #           print "--> acc:" + str(accrate),
            slink_corr.append( 1. / float(point[7]) * (1.0 - accrate)) 
 #           print 1. / float(point[7]) * (1.0 - accrate)
            
    slink_all = zip(slink_sbil,slink_corr)
  #  print slink_all
    plt.figure(1)
    plt.plot([x[0] for x in slink_all],[x[1] for x in slink_all],'ro')
    #make a linear fit through the points
    fit = np.polyfit([x[0] for x in slink_all if x[0] >3.3 and x[0] < 5],[x[1] for x in slink_all if x[0] >3.3 and x[0] < 5],1)
    func = np.poly1d(fit)
    functions.append(func)
#     #use fit as correction

#     #plot fit
    xp = np.linspace(interval[0], interval[1], (interval[1] - interval[0]) * 10)
    plt.figure(1)
    plt.plot(xp,func(xp),'r-')

    plotname = str(fill) + ".png"
    plt.savefig(plotname)
    plt.clf()


slink_slope = [x[1] for x in functions]
slink_all = zip(fill_numbers, slink_slope,functions)


# let's try to make some correlations between the slopes of VDManalyze and SLINK

for point in slink_all:
    for ii in all_all:
        if point[0] == ii[0]:
            plt.figure(2)
            plt.plot(ii[1],point[1],'ro')

            plt.figure(3)
            print ii[0]
            print ii[1]
            print point[2](ii[2])
            plt.plot(ii[1],point[2](ii[2]),'ro')


plt.figure(2)
plt.ylabel("SLINK slope")
plt.xlabel("VDMAna slope")

plt.figure(3)
plt.ylabel("SLINK correction factor @ avgSBIL of emittance scan")
plt.xlabel("VDMAna slope")



plt.show()



quit()


