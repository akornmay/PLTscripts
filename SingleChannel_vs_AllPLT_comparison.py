#!/usr/bin/python

import json
import time
import datetime
import numpy

#
# Load the PLTZERO data
#

#allPLTFileName = "/Users/Andreas/Work/PLT/2017_Data/PLTZERO.txt"
allPLTFileName = "/Users/Andreas/Work/PLT/2017_Data/6018-6084.json"

allPLTFile = open(allPLTFileName, 'r')
allPLTData = allPLTFile.read()
#parsedALL = json.loads(allPLTData[3:])
parsedALL = json.loads(allPLTData)

for sigVisData in parsedALL:
    if "PLT" in sigVisData['name']:
        print "Results for "+sigVisData['name']
        sigVisX = sigVisData['x']
        sigVisY = sigVisData['y']
        sigVisFill = sigVisData['_other']['fill']
        allPLTList = zip(sigVisFill,sigVisX,sigVisY,[ time.mktime(datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').timetuple()) for i in sigVisX])
        print allPLTList

#allPLTDict = dict((zip([time.mktime(datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').timetuple()) for i in sigVisX],sigVisY)))
allPLTDict = dict((zip(sigVisX,sigVisY)))
print allPLTDict

#
# Load the per channel data
#
#scPLTFileName = "../../2017_Data/PLTbyChannel.txt"
scPLTFileName = "../../2017_Data/sc_6018-6084.json"
mydicts = []

scPLTFile = open(scPLTFileName, 'r')
scPLTData = scPLTFile.read()
parsedSC = json.loads(scPLTData)

# We have some bad channels that are not used for the full lumi calculation

for sigVisData in parsedSC:
    if "PLT" in sigVisData['name'] and 'Const' not in sigVisData['name'] and 'PLT3:' not in sigVisData['name'] and 'PLT0:' not in sigVisData['name'] and 'PLT:SG' not in sigVisData['name']:
        print "Results for "+sigVisData['name']
        SCsigVisX = sigVisData['x']
        SCsigVisY = sigVisData['y']
        SCsigVisFill = sigVisData['_other']['fill']
        scPLTList = zip(SCsigVisFill, SCsigVisX, SCsigVisY,[time.mktime(datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').timetuple()) for i in SCsigVisX])
#        mydicts.append(dict(zip([time.mktime(datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S.%fZ').timetuple()) for i in SCsigVisX],SCsigVisY)))
        mydicts.append(dict(zip(SCsigVisX,SCsigVisY)))

#print mydicts
        

#
# First let's combine all the data from the single channels to a single list
# The fill number is not a good key since there can be multiple emittance scans in a fill, so we use the time stamp

scDict = {}

for dic in mydicts:
    for key, value in dic.iteritems():
        scDict.setdefault(key, []).append(value)

#print scDict
    
# now let's create a single average value for all keys

scAVGDict = {}

for key, value in scDict.iteritems():
    scAVGDict.setdefault(key,numpy.average(value))

print scAVGDict

# now let's just print things next to each other where we have a match
print "key;SC_avg;PLT_all;abs_diff;rel_diff"
for key, value in scAVGDict.iteritems():
    if key in allPLTDict:
#        print "key:" + key + " SC_avg:"  + str(value) + " PLT_all:" + str(allPLTDict[key]) + " abs_diff:" + str(abs(value - allPLTDict[key])) + " rel_diff:" + str((value/allPLTDict[key] - 1) *100)
        print datetime.datetime.strptime(key, '%Y-%m-%dT%H:%M:%S.%fZ').strftime("%d/%m/%y  %H:%M:%S") + ";" + str(value) + ";" + str(allPLTDict[key]) + ";" + str(abs(value - allPLTDict[key])) + ";" + str((value/allPLTDict[key] - 1) *100)









