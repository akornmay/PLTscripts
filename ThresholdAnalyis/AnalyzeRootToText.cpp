// Just a simple root script to create plots from the calibration routines

#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <TRandom3.h>
#include <TMath.h>
#include <TGraph.h>
#include <TH1.h>
#include <TProfile.h>


void analyzeRtT() {
  
  char filename[256] = "FinalTrimBits.root" ;
  
  TFile * file = new TFile(filename,"READ");
  if ( file->IsOpen()) printf("File opened successfully\n");				    
  
  ofstream outfile;
  outfile.open("TrimBitOut.txt");


  char histname[256];
  //  int channel[] = {2};
  int channel[] = {2,4,5,8,10,11,13,14,16,17,19,20,22,23}  ;

  char outtxt[256];

  for(int ii = 0; ii < sizeof(channel)/sizeof(int); ++ii) {
    //int roc = 1;
    for(int roc = 1; roc <= 3; ++roc) {
      //sprintf(histname,"ThresholdMap_Channel_%02i_ROC_%02i",channel,roc);
      sprintf(histname,"TrimBitMap_Channel_%02i_ROC_%02i",channel[ii],roc);
      cout << histname << endl;
      
      TH2S * currentHist;
      currentHist = (TH2S*)file->Get(histname);
      
      for(int col = 1; col <= 52; ++col)
	{
	  for(int row = 1; row <= 80; ++row)
	    {
	      sprintf(outtxt,"%i%i%02i%02i %i\n",channel[ii],roc,col-1,row-1,currentHist->GetBinContent(col,row));
	      outfile << outtxt;
	    }//row
	}//col
      delete currentHist;
    }//roc
  }//channel
  outfile.close();  

}//comparison
