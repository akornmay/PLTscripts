#!/usr/bin/python
import sys

#load the mapping file

with open('mapping.txt','r') as mapfile:
    channelmap = mapfile.readlines()

channelmap = [x.split() for x in channelmap]
channelmap = [[int(x[0])/10000,int(x[1])/10000] for x in channelmap]


#load trims to memory

with open('TrimBitOut.txt','r') as trimfile:
    trimmap = trimfile.readlines()

trimmap = [x.split() for x in trimmap]
trimmap = [[int(x[0]),x[1]] for x in trimmap]

#now read the trim bit text file a few times and create the trims

header = "5 0 0 0 0 0 0 0 0 0\n"


for channel in channelmap:
    #crate TrimsFile
    mfec = channel[1]/10000 
    mfecchannel = (channel[1]/1000)%10
    hubadd = (channel[1]/10)%100
    roc = channel[1]%10
    outfilename = "fasttrim_values_mFec" + str(mfec) + "_mFecChannel" + str(mfecchannel) + "_hubAddress" + str(hubadd) + "_roc" + str(roc) + ".pix1"

    print "Creating new file: " + outfilename
    outfile = open(outfilename, 'w')

    outfile.write(header)
    for ii in range(0,26):
        outfile.write("1\n")


    for pixel in trimmap:
        if pixel[0]/10000 == channel[0]:
            
            outfile.write(str((pixel[0]/100)%100) + " " + str(pixel[0]%100) + " 1 0 " + pixel[1] + "\n")


    outfile.close()
    

