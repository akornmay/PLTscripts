# python Processorquicklook.py /path/to/file.hdf5 channelNo

import tables, pandas as pd, pylab as py, sys, numpy


filen = sys.argv[1]
channel = sys.argv[2]

h5file = tables.open_file(filen)

#hist = h5file.root.bcm1fhist
#hist = h5file.root.bcm1flumi
hist = h5file.root.bcm1fagghist



#sumhist = numpy.zeros(14256)
sumhist = numpy.zeros(3564)
elements = 0
for row in hist.iterrows():
    mychannel =  int(row['channelid'])
    if int(row['channelid'])== int(channel):
#    if int(row['channelid']) != 1 and int(row['channelid']) != 2:
#    if mychannel != 1 and mychannel != 2 and mychannel >=1 and mychannel <13:
#    if mychannel != 1 and mychannel != 2 and mychannel >=13 and mychannel <25:
#    if mychannel != 1 and mychannel != 2 and mychannel >=1 and mychannel <25:
#    if mychannel != 1 and mychannel != 2 and mychannel >=25 and mychannel <37:
#    if mychannel != 1 and mychannel != 2 and mychannel >=37 and mychannel <49:
#    if mychannel != 1 and mychannel != 2 and mychannel >=25 and mychannel <49:
        sumhist = numpy.add(sumhist,row['data'])
#        sumhist = numpy.add(sumhist,row['bx'])
        elements = elements +1

h5file.close()

print 'Number of histograms found: '+ str(elements)

avghist = sumhist/elements

#print [x for x in avghist]

#py.plot(range(14256),avghist,'.-')
py.plot(range(3564),avghist,'.-')
py.grid()

py.xlabel('BX number')
py.ylabel('Average per-bunch lumi per orbit')

#py.title("File: "+filen, fontsize=10, verticalalignment='center')
py.title("File: "+filen, fontsize=10)

py.show()
