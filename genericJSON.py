#!/usr/bin/python

import json

jsonFileName = "./chartData.json"

jsonFile = open(jsonFileName, 'r')
jsonData = jsonFile.read()
parsedJSON = json.loads(jsonData[3:])

for lumData in parsedJSON:
    print "Results for "+lumData['name']
    for i in range(len(lumData['y'])):
