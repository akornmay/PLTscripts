read -p 'Please provide workloop timestamp from /brildata/plt/workloop_data/: ' tstamp
#tstamp=20160725.005211
wd=/scratch/akornmay/${tstamp}/
mkdir -p ${wd}
cd       ${wd}
gzips=/brildata/plt/workloop_data/*${tstamp}*gz
printf "\n%s\n" "unzipping..."
for file in ${gzips[@]}
# do cat  ${file} | gzip -d > $( echo ${wd}/${file##*/} | sed 's|.gz$||g' )
  do zcat ${file}           > $( echo ${wd}/${file##*/} | sed 's|.gz$||g' )
done
printf "\n%s\n" "processing..."
source       /brilpro/plt/cmsplt/setenv-prod.sh
PATH=${PATH}:/brilpro/plt/cmsplt/interface/bin/
check_transfile Data_Transparent_${tstamp}.dat   > Data_Transparent_${tstamp}.txt
check_scalfile  Data_Scaler_${tstamp}.dat        > Data_Scaler_${tstamp}.txt
histfile_check  Data_Histograms_${tstamp}_V2.dat > Data_Histograms_${tstamp}_V2.txt
printf "\n%s\n" "done! clean up unzipped files..."
rm -i ${wd}/Data_Transparent_${tstamp}.dat ${wd}/Data_Scaler_${tstamp}.dat ${wd}/Data_Histograms_${tstamp}_V2.dat
mv -v ${wd}/Data_Status_${tstamp}.dat      ${wd}/Data_Status_${tstamp}.txt
